import csv


import csv

def float_to_signed_8bit_binary(value):
    # Convert float to integer
    int_value = int(float(value))
    # Ensure the value is within the range of signed 8-bit integers
    if int_value < -128 or int_value > 127:
        raise ValueError(f"Value {int_value} out of range for signed 8-bit integer.")
    # Convert to 8-bit signed binary representation
    if int_value < 0:
        # Convert negative value to its two's complement binary form
        binary_value = format(256 + int_value, '08b')
    else:
        # Convert positive value to binary
        binary_value = format(int_value, '08b')
    return binary_value

def convert_csv_floats_to_signed_8bit_binary(input_file, output_file):
    with open(input_file, 'r') as infile, open(output_file, 'w', newline='') as outfile:
        csv_reader = csv.reader(infile)
        csv_writer = csv.writer(outfile)

        for row in csv_reader:
            # Convert each value in the row to signed 8-bit binary
            binary_row = [float_to_signed_8bit_binary(value) for value in row]
            # Write the converted row to the output file
            csv_writer.writerow(binary_row)

def main():
    input_file = 'data/binary_cnn_weights_conv1.weight.csv'
    output_file = f'output/{input_file}'
    convert_csv_floats_to_signed_8bit_binary(input_file, output_file)

if __name__ == "__main__":
    main()