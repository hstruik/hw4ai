import csv


def float_to_binary(value):
    # Convert float to integer
    int_value = int(float(value))
    # Convert to binary: 1 for 1, 0 for -1
    if int_value == 1:
        return '1'
    elif int_value == -1:
        return '0'
    else:
        raise ValueError(f"Value {int_value} is not valid for binary conversion. Only 1 and -1 are allowed.")


def convert_csv_to_binary_mif(input_file, output_file):
    weights = []
    with open(input_file, 'r') as infile:
        csv_reader = csv.reader(infile)
        for row in csv_reader:
            for value in row:
                weights.append(float_to_binary(value))

    depth = len(weights)
    width = 1  # Single bit per value

    with open(output_file, 'w') as file:
        file.write("DEPTH = {};\n".format(depth))
        file.write("WIDTH = {};\n".format(width))
        file.write("ADDRESS_RADIX = DEC;\n")
        file.write("DATA_RADIX = BIN;\n")
        file.write("CONTENT BEGIN\n")
        for i, weight in enumerate(weights):
            file.write("    {} : {};\n".format(i, weight))
        file.write("END;\n")


def main():
    input_file = 'data/binary_cnn_weights_conv1.weight.csv'
    output_file = 'output/mifs/weights.mif'
    convert_csv_to_binary_mif(input_file, output_file)


if __name__ == "__main__":
    main()
